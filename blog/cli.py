"""
cli.py - command line interface to hive static blog
Copyright (C) 2020  Richard T. Carback III

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

For alternative licensing, contact the author.

Contact the author at:
  - https://carback.us/rick
  - https://gitlab.com/carback1
"""
import click
import json
import os
import shutil
import pypandoc

from blog import hive, generate

def write_post(post, basedir="./"):
    """ write_post writes a blog post to "[created date]/[permlink].md """
    create_date = post['created'].split("T")[0]
    permlink = post['permlink']
    os.makedirs(create_date, exist_ok=True)
    outfn = os.path.abspath(f"{basedir}/{create_date}/{permlink}")
    with open(outfn, 'w') as outfile:
        # Write the header per steemit markdown prefs
        outfile.write("# {post['title']}\n\n")
        # TODO: Download CDN images, link rewriting, author and metadata info
        # Now the body, with all the headers shifted
        outfile.write(post['body'].replace("\n#", "\n##"))
        outfile.write("\n")

@click.group()
def main():
    pass

@main.command(help="Download posts from the network")
@click.option("--author", "-a", default="carback1",
              help="The hive user's blog to download")
@click.option("--nodesfile", "-n", default=None,
              type=click.File('r'),
              help="The line-separated list of hive nodes")
@click.option("--lastpermlinkfile", "-l", default=".lastpermlink",
              help="The last permlink we downloaded from the network")
@click.option("--output", "-o", default=click.get_text_stream('stdout'),
              type=click.File('a'),
              help="The output jsonl file, defaults to stdout")
def download(author, nodesfile, lastpermlinkfile, output):
    # Read in the nodes we should try to contact, 1 per line
    # The default are the hive nodes, but this can be redirected at steem
    # or a testnet.
    if nodesfile:
        nodes = [x.strip() for x in nodesfile.readlines()]
        hive.setup(nodes)

    # Open the lastpermlink file and grab the most recent permlink we
    # have seen when running this script.
    last_permlink = None
    if os.path.exists(lastpermlinkfile):
        with open(lastpermlinkfile, "r") as permlinkfile:
            last_permlink = permlinkfile.readline().strip()

    first_permlink = None
    for b in hive.get_posts(author):
        # If we've seen this one before, stop processing
        if last_permlink == b["permlink"]:
            break
        # If this is the first post, save this permlink to the permlink file
        # (the api always returns the newest post first)
        if not first_permlink:
            first_permlink = b["permlink"]
        # Write it to output as a jsonl
        output.write(json.dumps(b))
        output.write("\n")

    # TODO: Maybe re-open, sort the posts file, and make sure we are always
    #       recording the most recent post? It should work as is, but this
    #       would be more robust.
    if first_permlink:
        with open(lastpermlinkfile, "w") as permlinkfile:
            permlinkfile.write(first_permlink)

@main.command(help="Process hive downloads to produce a json index with posts.")
@click.option("--output-dir", "-o", default=".",
              help="The output directory, defaults to CWD")
@click.option("--infile", "-i", default=click.get_text_stream('stdin'),
              type=click.File('r'),
              help="The input jsonl file, defaults to stdin")
@click.option("--maxposts", "-m", default=15,
              type=int,
              help="The max # of post summaries in a given month to print")
def process(output_dir, infile, maxposts):
    # Generate site index files
    post_index = generate.index(infile, output_dir)

    # Output indexes for All (all.md), Years ([Year]/index.md), and
    # Months ([Year]/[Month]/index.md)
    # If any category has more than maxposts, then print how many posts,
    # otherwise print post summaries
    generate.save_index_mds(post_index, 15, output_dir)

@main.command(help="Convert markdown into html with the given templates")
@click.option("--directory", "-d", default=".",
              help="The directory to process, defaults to CWD")
@click.option("--css", "-s", default="style.css",
              help="Cascading style sheet to use (copied into directory)")
@click.option("--template", "-t", default="template.html",
              help="The template for ")
def publish(directory, css, template):
    base_dir = os.path.abspath(os.path.expanduser(directory))
    css_dst = os.path.join(base_dir, os.path.basename(css))
    css_src = os.path.abspath(os.path.expanduser(css))

    # Copy the style into the publishing path, if not exists there already
    if css_src != css_dst:
        shutil.copyfile(css_src, css_dst)

    # Run pandoc for each markdown file with the given template and style
    css_fn = os.path.basename(css_dst)
    for dpath, _, fnames in os.walk(directory):
        for fn in fnames:
            if not fn.endswith(".md"):
                continue
            rel_path = os.path.relpath(base_dir, dpath)
            css_path = os.path.join(rel_path, css_fn)
            out_fn = fn.replace(".md", ".html")
            src_path = os.path.join(dpath, fn)
            dst_path = os.path.join(dpath, out_fn)
            args = [f"--css={css_path}", f"--template={template}"]
            print(dst_path)
            html = pypandoc.convert_file(src_path, to="html", format="markdown",
                                         outputfile=dst_path, extra_args=args)


# TODO: download -> index (into json) -> build (json -> markdown)
#       -> publish
# The idea behind this workflow is that you can insert and do a different
# thing at each step and create different workflows (i.e., use json direct
# for a single page app/SPA OR use the markdown to create PDFs/a book, etc)
if __name__ == '__main__':
    main()
