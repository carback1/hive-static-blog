"""
generate.py - generate ephemeral content for date records and web pages
Copyright (C) 2020  Richard T. Carback III

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

For alternative licensing, contact the author.

Contact the author at:
  - https://carback.us/rick
  - https://gitlab.com/carback1
"""

import json
import os
import pypandoc
import yaml
import re

from datetime import datetime

base_url = "https://hive.blog"

def generate_metadata(post):
    """ generate_metadata creates a pandoc yaml metadata section for a post """
    global base_url
    meta = {
        "title": post["title"],
        "author": post["author"],
        "created": post["created"],
        "last_update": post["last_update"],
        "keywords": post["metadata"]["tags"],
        "hive": {
            "link": base_url + post["url"].replace("@", "\@"),
            "name": base_url.split("://")[1]
        },
        "navlinks": {
            "up": "../"
        }
    }
    if post["prev"] is not None:
        meta["navlinks"]["prev"] = post["prev"]
    if post["next"] is not None:
        meta["navlinks"]["next"] = post["next"]

    return f"---\n{yaml.dump(meta)}\n---\n\n"

def create_post(post_json, location):
    body = post_json["body"]
    # Fix weird formatting with images where it starts w/ a space,
    # which became a newline in md file
    body = body.replace("![ \"", "![\"")
    # TODO: Insert other post-process things here
    lines = list()
    for line in body.split("\n"):
        tline = line.strip()
        if tline.startswith("http") and tline.endswith(".mp4"):
            vsrc = f"""<source src="{tline}" type="video/mp4">"""
            line = f"<video controls>{vsrc}</video>"
        lines.append(line)
    body = "\n".join(lines)

    fmt = post_json["metadata"]["format"]
    url = base_url + post_json["url"]

    content = generate_metadata(post_json)
    content += pypandoc.convert_text(body, "markdown", fmt)

    with open(os.path.join(location, "index.md"), "w") as outmd:
        outmd.write(content)

def create_post_summary(post_json, size=300):
    """
    create_post_summary processes a post and creates summary text

    :param post: the post dict
    :returns: the post summary
    """
    imgre = re.compile('''!\[[^\]]*\]\((?P<fname>.*?)(?=\"|\))(?P<optionalpart>\".*\")?\)''')
    body = post_json['body']
    fmt = post_json["metadata"]["format"]
    txt = pypandoc.convert_text(body, "plain", fmt)
    summary = ""
    # Note: not clear if line endings will differ on other platforms here...
    for para in txt.split("\n\n"):
        # Strip image tags
        para, cnt = re.subn(imgre, "", para)
        # Strip newlines and whitespaces at the end of the new lines -> 1 space
        para = " ".join([p.strip() for p in para.split("\n")])
        # Skip if it has less than a sentence
        if para.find(". ") == -1:
            continue

        summary = para

        # If the summary is too big, clip it to the size
        if len(summary) >= size-1:
            summary = summary[0:size-1]
            summary += "…"

        break

    return summary

def load_post_json(location, basedir=""):
    """ load_post_json returns the post json for a given location """
    dirname = os.path.abspath(os.path.join(basedir, location))
    with open(os.path.join(dirname, "index.json"), "r") as postfile:
        return json.loads(postfile.read())

def save_post_json(post, location, basedir=""):
    """ save_post_json saves the post json for a given location """
    dirname = os.path.abspath(os.path.join(basedir, location))
    os.makedirs(dirname, exist_ok=True)
    path = os.path.join(dirname, "index.json")
    with open(path, "w") as postfile:
        postfile.write(json.dumps(post))
    return os.path.relpath(dirname, basedir)

def index(posts, basedir="."):
    """
    index organizes a jsonl file of posts into directories:

      [Year]/[Month]/[permlink]/index.json

    Each post json file has a previous and next element added that points
    to the previous and next json file, or None if there aren't any.

    :param posts: iterable of blog posts in json format
    :returns: a map of created dates to post summaries with file locations
    """
    post_index = dict()
    for post_json in posts:
        post = json.loads(post_json)

        # Fixup attributes here
        # First, convert json_metadata -> metadata
        post["metadata"] = json.loads(post["json_metadata"])
        del post["json_metadata"]

        created = datetime.fromisoformat(post["created"])
        permlink = post["permlink"]
        location = f"{created.year}/{created.month:02}/{permlink}"

        post_index[created] = save_post_json(post, location, basedir)

    # Create prev/next links
    post_dates = sorted(post_index.keys())
    for i in range(len(post_dates)):
        location = post_index[post_dates[i]]
        post = load_post_json(location, basedir)

        prev_post = None
        next_post = None
        if i > 0:
            prev_post = os.path.relpath(post_index[post_dates[i-1]], location)
        if i < len(post_dates)-1:
            next_post = os.path.relpath(post_index[post_dates[i+1]], location)
        post["prev"] = prev_post
        post["next"] = next_post

        create_post(post, os.path.join(basedir, location))
        save_post_json(post, location, basedir)

    return post_index

def get_md_link(text, link):
    """ get_md_link returns the string to create a mark down link """
    return f"""<a href="{link}">{text}</a>"""

def get_summary_md(location, basedir):
    """ get_md_summary returns a markdown representation of a post summary"""
    post = load_post_json(os.path.join(basedir, location))
    title = post["title"]
    img = post["metadata"].get("image", [])
    summary = create_post_summary(post)

    img_html = ""
    if img:
        img = img[0]
        img_html = f"""<div class="title-image"><img src="{img}" /></div>"""
    title_html = f"""<div class="title">{title}</div>"""
    sum_html = f"""<div class="summary">{summary}</div>"""

    p = f"""<div class="post-summary">{img_html}{title_html}{sum_html}</div>"""
    return get_md_link(p, location)

def get_summaries_md(locations, basedir):
    """ get_summaries_md returns a list of summaries in markdown"""
    md = list()
    for s in locations:
        mdsum = get_summary_md(s, basedir)
        md.append(f"* {mdsum}")
    return md

def get_month_md(month, year, posts, basedir):
    """ get_month_md generates the markdown for a month index page """
    month_name = datetime(year, month, 1).strftime("%B")
    month_str = f"{month:02}"
    md = f"# Posts for {month_name}, {year}\n\n"
    yearlink = get_md_link(f"(see all posts in {year})", "../")
    md += f"{yearlink}\n\n"
    # make relatively pathed link
    basedir = os.path.join(basedir, f"{year}/{month_str}/")
    posts = [p.replace(f"{year}/{month_str}/", "") for p in posts]
    md += "\n".join(get_summaries_md(posts, basedir))
    md += "\n"
    return md

def get_year_md(year, posts, basedir):
    """ get_year_md generates the markdown for a year index page """
    md = f"# Posts for {year}\n\n"

    all_link = get_md_link(f"(see all posts)", "../all.html")
    md += f"{all_link}\n\n"
    basedir = os.path.join(basedir, f"{year}")
    for month, locs in posts.items():
        month_name = datetime(year, month, 1).strftime("%B")
        month_str = f"{month:02}"
        mlink = get_md_link(month_name, month_str)
        md += f"* {mlink}\n"
        # convert locs to relative path
        locs = [l.replace(f"{year}/", "") for l in locs]
        sum_mds = { f"  {s}" for s in get_summaries_md(locs, basedir) }
        md += "\n".join(sum_mds)
        md += "\n"
    return md

def get_all_md(posts, basedir):
    """ get_all_md generates the markdown for all posts index page """
    md = f"# Index of All Posts\n\n"

    for year, month_posts in posts.items():
        ylink = get_md_link(f"{year}", f"{year}")
        md += f"* {ylink}\n"
        for month, locs in month_posts.items():
            month_name = datetime(year, month, 1).strftime("%B")
            month_str = f"{month:02}"
            mlink = get_md_link(month_name, os.path.join(f"{year}", month_str))
            md += f"  * {mlink}\n"
            sum_mds = { f"    {s}" for s in get_summaries_md(locs, basedir) }
            md += "\n".join(sum_mds)
            md += "\n"
    return md

def get_index_md(posts, basedir, max_cnt = 5):
    """ get_index_md prints the last 5 posts """
    md = f"# Most Recent Blog Posts\n\n"
    cnt = 0

    for year, month_posts in posts.items():
        for month, locs in month_posts.items():
            month_str = f"{month:02}"
            for s in locs:
                if cnt < max_cnt:
                    md += "\n\n"
                    md += get_summary_md(s, basedir)
                    md += "\n\n----------\n\n"
                    cnt += 1
                else:
                    break
            if cnt >= max_cnt:
                break
        if cnt >= max_cnt:
            break
    return md


def save_index_mds(posts_index, max_posts=15, basedir="."):
    """
    save_index_mds will output Markdown indexes for All (all.md),
    Years ([Year]/index.md), and Months ([Year]/[Month]/index.md)

    If any Month category has more than maxposts, then print how many posts,
    otherwise print post summaries
    """
    idx = dict()
    for pdate in sorted(posts_index.keys(), reverse=True):
        summary = posts_index[pdate]
        if pdate.year not in idx:
            idx[pdate.year] = dict()
        if pdate.month not in idx[pdate.year]:
            idx[pdate.year][pdate.month] = list()
        idx[pdate.year][pdate.month].append(summary)

    dirname = os.path.abspath(basedir)
    with open(os.path.join(dirname, "all.md"), "w") as allpage:
        allpage.write(get_all_md(idx, dirname))

    with open(os.path.join(dirname, "index.md"), "w") as ipage:
        ipage.write(get_index_md(idx, dirname))

    for year, months_idx in idx.items():
        year_path = os.path.join(dirname, f"{year}", "index.md")
        with open(year_path, "w") as yearpage:
            yearpage.write(get_year_md(year, months_idx, dirname))
        for month, locs in months_idx.items():
            month_path = os.path.join(dirname, f"{year}", f"{month:02}",
                                      "index.md")
            with open(month_path, "w") as monthpage:
                monthpage.write(get_month_md(month, year, locs, dirname))
