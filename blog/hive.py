"""
hive.py - download blog entries from hive/steem/etc
Copyright (C) 2020  Richard T. Carback III

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

For alternative licensing, contact the author.

Contact the author at:
  - https://carback.us/rick
  - https://gitlab.com/carback1
"""

import steem
import time

from steembase.chains import known_chains

# This is the HIVE network definition required to connect to hive nodes
known_chains['HIVE'] = {
        "chain_id": "0" * int(256 / 4),
        "prefix": "STM",
        "hive_symbol": "HIVE",
        "hbd_symbol": "HBD",
        "vests_symbol": "VESTS",
    }

# These are a relatively recent list of hive nodes
hive_nodes = [ "https://api.pharesim.me", "https://rpc.ausbit.dev",
               "https://api.hive.blog", "https://anyx.io",
               "https://api.openhive.network",
               "https://hived.privex.io", "https://api.hivekings.com",
               "https://rpc.esteem.app", "https://techcoderx.com" ]

net = None

def setup(nodes=hive_nodes):
    """
    setup sets up the hive network object

    :param nodes: the list of nodes to use
    """
    global net
    net = steem.Steem(nodes)

def get_posts(author, start_permlink="", limit=10, rate=2):
    """
    get_posts downloads all blog posts for an author and returns them
    via generator (for b in hive_download(...)) to iterate over.

    Note also that the "before_date" option in the steem API is broken,
    otherwise this code would be cleaner. See the following code for why:

    https://github.com/steemit/steem/blob/b2c9ae8/libraries/plugins/apis/tags_api/tags_api.cpp#L480

    :param author: The hive username (without the @ symbol in front)
    :param start_permlink: work backwards from this post permlink (usually
                       a lowercase title with - instead of spaces, e.g.,
                       "First Post" has permlink: "first-post")
    :param limit: how many posts to download at once, typically 10 is good
    :param rate: how many seconds to wait between downloads
    :returns: generator of blog post dictionaries to iterate over
    """
    global net
    if net is None:
        setup()

    more_data = True
    last_permlink = start_permlink
    while more_data:
        blogs = net.get_discussions_by_author_before_date(author,
                                                          start_permlink,
                                                          "",
                                                          limit)
        for b in blogs:
            if b["permlink"] == last_permlink:
                continue
            last_permlink = b["permlink"]
            yield b
        time.sleep(rate)
        if len(blogs) < limit:
            more_data = False
        start_permlink = last_permlink
