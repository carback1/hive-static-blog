"""setup.py -- setup script

Setuptools config
"""

from setuptools import setup

setup(
    name='blog',
    version='0.0.1',
    packages=['blog'],
    install_requires=[
        'steem',
        'pypandoc',
        'pandocfilters',
        'click'
    ],
    entry_points={
       'console_scripts': [
           'blog = blog.cli:main'
       ]
    }
)
